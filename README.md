## 基本功能
1. 基本的筆刷 : Pencil Mode。
2. 基本的橡皮擦 : 點選Eraser圖示即可使用。
3. 顏色選擇器 : 可以選擇線條顏色(line color)和陰影顏色(shadow color)。
4. 筆刷大小 : 移動line width 的bar條，選取對應的大小。
5. 文字 : 點擊"點按添加文字"的按鈕，新增一段可自由修改內容的文字。Font-family選擇希望的字體。Font-size決定當前文字物件的字體大小。
6. 游標 : 當鼠標移動到畫布上會變成十字形。
7. 重新整理 : 點選"clear"按鈕清除畫布。
8. 點三角形、圓形、正方形圖示可以拉出對應的圖案。
9. undo、redo : 點選<img src="image/redo.png" width="20" height="20"> <img src="image/undo.png" width="20" height="20">。
10. 上傳、下載 : 點選"上傳圖片"、"Save as image"按鈕。


## 其他功能
1. 可以選擇筆刷的陰影大小、顏色。
2. 有多種不同的筆刷類型(Mode)可以選擇，例如circle、spray...。
3. 可以調整物件在圖片中的位置，例如:文字...。

